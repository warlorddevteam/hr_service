package com.renegatumsoulteam.hr.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.concurrent.Future;


@Service
public class ParserService {

    public ParserService(String url) {
        this.url = url;
    }

    public ParserService() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String url;

    @Async
    public Future<Integer> pars() throws InterruptedException {
        Document doc = null;
        try {
            doc = Jsoup.connect(this.url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert doc != null;
        Elements ntmDivs = doc.getElementsByClass("notice-status");
        return AsyncResult.forValue(ntmDivs.size());
    }
}
