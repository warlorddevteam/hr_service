package com.renegatumsoulteam.hr.controllers;

import com.datastax.driver.core.utils.UUIDs;
import com.renegatumsoulteam.hr.entity.Customer;
import com.renegatumsoulteam.hr.models.CustomerModel;
import com.renegatumsoulteam.hr.services.ParserService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

@Component
@Path("")
public class HelloWorld {

    private CustomerModel repository;
    private ParserService parserService = new ParserService();

    @Path("/")
    @GET
    public Response hello() {
        return Response.ok("hi").build();
    }

    @Path("test")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response test() {
        this.repository.deleteAll();

        // save a couple of customers
        this.repository.save(new Customer(UUIDs.timeBased(), "Alice", "Smith"));
        this.repository.save(new Customer(UUIDs.timeBased(), "Bob", "Smith"));

        // fetch all customers
        System.out.println("Customers found with findAll():");
        System.out.println("-------------------------------");
        for (Customer customer : this.repository.findAll()) {
            System.out.println(customer);
        }
        System.out.println();

        // fetch an individual customer
        System.out.println("Customer found with findByFirstName('Alice'):");
        System.out.println("--------------------------------");
        System.out.println(this.repository.findByFirstName("Alice"));

        System.out.println("Customers found with findByLastName('Smith'):");
        System.out.println("--------------------------------");
        for (Customer customer : this.repository.findByLastName("Smith")) {
            System.out.println(customer);
        }
        return Response.ok("hui").build();
    }


    @Path("notest")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response notest() throws IOException, ExecutionException, InterruptedException {
        Integer a = this.parserService.pars().get();
        return Response.ok(a).build();
    }
}